<?php

        session_start();

        $username = htmlspecialchars($_REQUEST['login'],ENT_QUOTES);  
        $password = htmlspecialchars($_REQUEST['password'],ENT_QUOTES);

        try{
            $bdd = new PDO('mysql:host=localhost;dbname=calendar;charset=utf8', 'root', 'isencir');
	}
        catch(Exception $e){
            die('Erreur : '.$e->getMessage());
        }

        $result = $bdd->prepare("SELECT * FROM Users WHERE login = ?");
        $result->execute(array($username));
        $data = $result->fetch();
        if($password === $data['password']){
            $_SESSION['login'] = $data['login'];
            header("Location:calendar_custom.php");
            exit();
        }
        else{
            $_SESSION['error'] = "LOGIN or PASSWORD is invalid.";
            header("Location:affiche.php");
            exit();
        }
$result->closeCursor();