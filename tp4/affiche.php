<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css_chat.css" type="text/css"/>
        <meta charset="utf-8" />
        <title>Calendrier de Tim</title>
    </head>
    <body>
        <header>
            Calendrier de Tim
        </header>
        <form action="session.php" method="post" enctype="multipart/form-data">
            <label>Login : </label><input type="text" name="login" required="" class="box"/><br /><br />
            <label>Password  : </label><input type="password" required="" name="password" class="box" /><br/><br />
            <?php 
            session_start();
            if(!empty($_SESSION['error'])){
                echo "<br>";
                echo htmlspecialchars($_SESSION['error']);
                echo "<br>"; 
                $_SESSION['error'] = "";
            }
            ?>
            <input type="submit" name='submit' value="Connexion" class='submit'/><br />
        </form>
    </body>
</html>