<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css_chat.css" type="text/css"/>
        <title>Mon Chat</title>
    </head>
    <body>
        <header>
            Chat de Tim
        </header>
        <br>
    <form action="chat_post.php" method="post">
        <p>
            <label for="nom">Nom</label> : <input type="text" name="nom" required /><br />
        <br>
        <label for="message">Message</label> :  <input type="text" name="message" required /><br />
        <br>
        <input type="submit" value="Partager" />
	</p>
    </form>
        <br>
<?php

try
{
	$bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', 'isencir');
}
catch(Exception $e)
{
        die('Erreur : '.$e->getMessage());
}


$reponse = $bdd->query('SELECT nom, message, date FROM messages ORDER BY ID DESC LIMIT 0, 10');


while ($donnees = $reponse->fetch())
{
	echo '<em>' . $donnees['date'] . '</em>' . ' : ' . '<strong>' . htmlspecialchars($donnees['nom']) . '</strong>' . ' : ' . htmlspecialchars($donnees['message']) . '</p>';
}

$reponse->closeCursor();

?>
    </body>
</html>